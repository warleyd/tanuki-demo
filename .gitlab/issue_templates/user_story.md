# User Story

## Title
<!-- Please provide a title for the User Story -->
Lorem ipsum dolor sit amet.

## Story
<!-- Write the story in the format of:
Who is this story meant for
What is that persona trying to do
Why is this needed -->

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Other tasks
- [ ] Assign appropriate labels
- [ ] Select appropriate due date


